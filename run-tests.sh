#!/bin/sh

echo "$GOOGLE_API_KEY" | base64 -d > /tmp/key.json
gcloud auth activate-service-account bitbucket-pipelines@poc.iam.gserviceaccount.com --key-file=/tmp/key.json --project=ptoject_name
gcloud container clusters get-credentials dev-cluster --zone=europe-west1-b

ENV_NAME=$1
POD_NAME=$(kubectl get pods -o=name | grep automationtesting-api)
kubectl exec -it "$POD_NAME" -- npm run location-api-mocha -- -env="$ENV_NAME" -- -storeReports --cloud